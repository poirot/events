# Events

Easy To Track And Structured Event Management System.

```php
$event = new BaseEvent('event-name');

// Attach Listener with "listener-name" at priority 100
$event->attachListener(new FunctorListener(function(){
    return ['template' => 'Hello %s, welcome!'];
}), 'listener-name', 100);
// ....................................................

// build an existence event with array config
(new EventBuilder([
    'listeners' => [
        'other-listener-name' => [
            'priority' => 90,
            'listener' => new FunctorListener(function($template = null) {
                $result = sprintf($template, 'Dear Developer');

                return ['result' => $result];
            }),
        ],
    ],
    'then' => function($event) {
        // Hello Dear Developer, welcome!
        echo $event->collector()->getResult();
    }
]))->build($event);
// ..........................................

$emitter = $event->emit(); // Hello Dear Developer, welcome!

## get data from collector
echo "<p>template:{$emitter->collector()->getTemplate()}</p>";
echo "<p>output:{$emitter->collector()->getResult()}</p>";

$collector = $emitter->collector();
$emitter->then(function($event) use ($collector) {
    if ($event->collector() === $collector)
        echo '<p>You see this, because collector has emitted data.</p>';
});

if ($collector === $event->collector())
    // this line never show, because event collector has only default values
    die('Collector is empty');

die();
```