<?php
namespace Poirot\Events\Interfaces;

use Poirot\Events\Event\DataCollector;

/**
 * Meeter Provide Logic Of Executing Listeners
 * Out of the Events Itself.
 * 
 * exp.
 *  - inject listener object dependencies 
 *  - resolve argument for listeners
 *  - skip execution of listeners if collection result achieved
 *
 */
interface iMeeter
{
    /**
     * Set Parent Event That Met
     *
     * @param iEvent $event
     * @return $this
     */
    function setEventBelong(iEvent $event);

    /**
     * Invoke Given Listener
     * 
     * @param callable      $listener
     * @param DataCollector $dataCollector Clone/Copy of Collector Instance
     * 
     * @return null|array|\Traversable null mean skip result
     */
    function invokeListener($listener, DataCollector $dataCollector);
    
    /**
     * Reset to emit new listener
     * 
     * ! each time event try to run new listener
     *   it will clone the meeter object 
     */
    function __clone();
}
