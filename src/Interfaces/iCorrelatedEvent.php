<?php
namespace Poirot\Events\Interfaces;


interface iCorrelatedEvent
{
    /**
     * Attach To Event
     * 
     * @param iEvent $event
     * 
     * @return $this
     */
    function attachToEvent(iEvent $event);
}
