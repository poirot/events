<?php
namespace Poirot\Events\Interfaces\Respec;

use Poirot\Events\Interfaces\iEvent;

interface iEventProvider
{
    /**
     * Get Events
     *
     * @return iEvent
     */
    function event();
}
