<?php
namespace Poirot\Events\Interfaces\Respec;

use Poirot\Events\Interfaces\iEvent;

interface iEventAware
{
    /**
     * Set Target Events
     *
     * @param iEvent $event
     *
     * @return $this
     */
    function setEvent(iEvent $event);
}
