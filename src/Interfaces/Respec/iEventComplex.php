<?php
namespace Poirot\Events\Interfaces\Respec;

interface iEventComplex
    extends
    iEventAware,
    iEventProvider
{ }
