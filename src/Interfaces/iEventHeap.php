<?php
namespace Poirot\Events\Interfaces;

use Poirot\Std\Interfaces\Struct\iData;

/**
 * It's a group of events
 *
 * exp. we can have ApplicationEvents
 *      that contains:
 *        BootstrapEvent -> with own options meta data and listeners attached into
 *        RouteEvent ...
 *        ...
 *        RenderEvent
 *        .
 *        .
 */
interface iEventHeap 
    extends iEvent
{
    /**
     * Bind an event to this event
     *
     * @param iEvent      $event
     *
     * @throws \Exception If Events With Name Exists
     * @return $this
     */
    function bind(iEvent $event);

    /**
     * Bind an event to this event and share collector
     *
     * @param iEvent $event
     *
     * @return $this
     */
    function bindShare(iEvent $event);

    /**
     * Find an event object by name
     *
     * - if iEvent passed as first argument register
     *   and return it
     *
     * @param string|iEvent $event
     *
     * @throws \Exception event not found
     * @return iEvent
     */
    function byEvent($event);

    /**
     * Helper To Attach Listener To Events(s).
     *
     * ! to attach listener to all events use getEvents method
     *
     * - if iEvent passed as argument register it if not exists
     *
     *  - on(['login', 'logout'], $logUserAccess)
     *  - on('login', $logUserAccess)
     *  - on(iEvent, ..)
     *
     * @param array|string|iEvent $event
     * @param callable            $listener
     * @param int $priority
     *
     * @throws \Exception event name not registered
     * @return $this
     */
    function on($event, $listener, $priority = 0);

    /**
     * Helper To Emit Events Listener
     *
     * - if iEvent passed as argument register it if not exists
     *
     *  - trigger(['login', 'logout'], $logUserAccess)
     *  - trigger('login', $logUserAccess)
     *  - trigger(iEvent, ..)
     *
     * note: return current emitter
     *
     * @param array|string|iEvent $event
     * @param array|iData|iMeeter $bp
     * @param iMeeter|null        $pb
     *
     * @return $this
     */
    function trigger($event, $bp = null, $pb = null);

    /**
     * Un-Register an event
     *
     * @param string|iEvent $event
     *
     * @return $this
     */
    function unbind($event);

    /**
     * Shutdown all listeners attached to all events
     *
     * @return $this
     */
    function shutdown();

    /**
     * Is Shutdown
     *
     * @return bool
     */
    function isShutdown();

    /**
     * Has Events
     *
     * @param string|iEvent $event
     *
     * @return boolean
     */
    function has($event);

    /**
     * Get All Registered Events Name
     *
     * note: mostly used for debug purposes
     *
     * @return array[string]
     */
    function listEvents();
}
