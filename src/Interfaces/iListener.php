<?php
namespace Poirot\Events\Interfaces;

use Poirot\Std\Interfaces\Pact\ipInvokable;

interface iListener 
    extends ipInvokable
{
    /**
     * Fire up action when event listener triggered
     *
     * - if we need back result into Events, must implement-
     *   iEventAware to inject event target into listener,
     *   then you can use setter/getter methods from target-
     *   event.
     *
     * ! stop propagation:
     *   $targetEvent->stopPropagation()
     *
     * @param null $argument
     *
     * @return mixed
     */
    function __invoke($argument = null);
}
