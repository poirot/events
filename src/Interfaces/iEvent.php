<?php
namespace Poirot\Events\Interfaces;

use Poirot\Std\Interfaces\Pact\ipMetaProvider;

use Poirot\Events\Event\DataCollector;


interface iEvent 
    extends ipMetaProvider
{
    /**
     * Event Entitle Name
     *
     * @return string
     */
    function getName();

    /**
     * Events Result Collector
     *
     * @return DataCollector
     */
    function collector();

    /**
     * Attach Listener To This Events
     *
     * [code:]
     *   attachListener($listener, $priority)
     *   attachListener($listener, $nameOverride, $priority)
     * [code]
     *
     * - store attached listener so we can list theme back
     * - listener with same name ({$name}) not allowed
     * - can`t Attach Listener While Emit Events
     *
     * @param callable    $listener
     * @param int |string $name      priority, null | name, priority
     * @param null|int    $priority            null |       priority
     *
     * @throws \RuntimeException Listener exists
     * @return $this
     */
    function attachListener($listener, $name = 0, $priority = 10);

    /**
     * Find For Listener with name or object
     *
     * @param iListener|string $listener
     *
     * @throws \InvalidArgumentException
     * @return iListener|false
     */
    function findListener($listener);

    /**
     * Detach A Listener From List
     *
     * - to detach by name you have to findListener() first
     *
     * @param iListener $listener
     *
     * @return $this
     */
    function detachListener(iListener $listener);

    /**
     * Detach Whole Listeners
     *
     * @return $this
     */
    function detachAll();

    /**
     * Trigger Listeners Within Events
     *
     * - reset propagation value
     * - iterate over listeners priority and fire actions
     * - readable props of this event can resolved as listener
     *   arguments.
     * - this event can be resolved to listener as $e or $event __invoke($e)
     * - implement to know last emitted listener
     *
     * note: collecting listeners results can implemented with meeter watch
     * note: each event after emit has collected of data result in Emitter
     *       Collector. if you want it can be merged with event collector.
     *       $results = $emitter->collector()
     *       $emitter->event()->collector()->from($result)
     *
     * @param array|\Traversable|iMeeter $dataMerge
     * @param iMeeter|null $meeter
     *
     * @return iEvent Clone/Copy of self
     * @throws \Exception
     */
    function emit($dataMerge = null, $meeter = null);

    /**
     * callable:
     *  mixed function(DataCollector $result, $eventSelf)
     *
     * @param callable $callable
     * @param callable $failure
     *
     * @return mixed|$this if callable not given
     * @throws \Exception
     */
    function then(/*callable*/ $callable = null, /*callable*/ $failure = null);

    /**
     * callable:
     *  mixed function(\Exception $exception, $eventSelf)
     *
     * @param callable $callable
     *
     * @return $this
     * @throws \Exception
     */
    function onFailure(/*callable*/$callable);

    /**
     * Stop Listeners Propagation
     *
     * @return $this
     */
    function stopPropagation();

    /**
     * Is Stopped?
     *
     * @return boolean
     */
    function isStopPropagation();

    /**
     * Get Last Emitted Listener Object
     *
     * @return null|iListener
     */
    function lastEmitted();
    
    /**
     * Get All Attached Listeners To This Events
     *
     * - listeners list must contain both
     *   priority and listener object
     *
     * note: can used for debug purposes
     *
     * @return iListener[]
     */
    function listListeners();


    // options:

    /**
     * Set Default Meeter Into Emit Method
     *
     * @param iMeeter $meeter
     *
     * @return $this
     */
    function setMeeter(iMeeter $meeter);
}
