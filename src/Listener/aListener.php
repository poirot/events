<?php
namespace Poirot\Events\Listener;

use Poirot\Std\ConfigurableSetter;

use Poirot\Events\Interfaces\iListener;

abstract class aListener 
    extends ConfigurableSetter
    implements iListener
{
    /**
     * Fire up action when event listener triggered
     *
     * - if we need back result into Events, must implement-
     *   iEventAware to inject event target into listener,
     *   then you can use setter/getter methods from target-
     *   event.
     *
     * ! stop propagation:
     *   $targetEvent->stopPropagation()
     *
     * @param null $argument
     *
     * @return mixed
     */
    abstract function __invoke($argument = null);
}
