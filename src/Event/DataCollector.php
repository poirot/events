<?php
namespace Poirot\Events\Event;

use Poirot\Std\Interfaces\Struct\iDataOptions;
use Poirot\Std\Struct\DataOptionsOpen;


/**
 * Data Transfer Object While Listeners Triggered On Same Event
 *
 * each event may have different specific options defined as a
 * setter/getter property here on collector.
 *
 */
class DataCollector
    extends DataOptionsOpen
    implements iDataOptions
{
    /**
     * Events with specific result must implement this object
     */
}
