<?php
namespace Poirot\Events\Event;


/**
 * This is the shortcut helper to assert listener result
 * and do things with emitter.
 * 
 * callable:
 *  mixed function(mixed $result, Meeter $meeter)
 *  $result is result of invoked listener
 *  $meeter meeter self
 *  return filtered result
 */
class MeeterCallable 
    extends Meeter
{
    /** @var callable */
    protected $callable;


    /**
     * MeeterCallable constructor.
     * @param $callable
     */
    function __construct($callable)
    {
        if (!is_callable($callable))
            throw new \InvalidArgumentException(sprintf(
                'Meeter get callable as argument; given: (%s).'
                , \Poirot\Std\flatten($callable)
            ));

        $this->callable = $callable;
    }

    /**
     * Invoke Given Listener
     *
     * @param callable      $listener
     * @param DataCollector $dataCollector Clone/Copy of Collector Instance
     *
     * @return null|array|\Traversable null mean skip result
     */
    function invokeListener($listener, DataCollector $dataCollector)
    {
        $result = parent::invokeListener($listener, $dataCollector);
        $result = call_user_func($this->callable, $result, $this);
        return $result;
    }
}
