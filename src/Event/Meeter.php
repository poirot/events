<?php
namespace Poirot\Events\Event;

use Poirot\Events\Interfaces\iEvent;
use Poirot\Events\Interfaces\iMeeter;


/**
 * Meeter Provide Logic Of Executing Listeners
 * Out of the Events Itself.
 *
 * exp.
 *  - inject listener object dependencies
 *  - resolve argument for listeners
 *  - skip execution of listeners if collection result achieved
 *
 * @see Event::emit
 */
class Meeter
    implements iMeeter
{
    /** @var iEvent */
    protected $event;

    /**
     * Set Parent Event That Met
     *
     * @param iEvent $event
     * @return $this
     */
    function setEventBelong(iEvent $event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return iEvent
     */
    function event()
    {
        return $this->event;
    }

    /**
     * Invoke Given Listener
     *
     * @param callable      $listener
     * @param DataCollector $dataCollector Clone/Copy of Collector Instance
     *
     * @return null|array|\Traversable null mean skip result
     */
    function invokeListener($listener, DataCollector $dataCollector)
    {
        ## resolve to listener arguments
        $preparedCallable = $this->_resolveToCallable($listener, $dataCollector);
        $result           = call_user_func($preparedCallable);
        return $result;
    }


    // ..

    protected function _resolveToCallable(/*callable*/$l, $collector)
    {
        ### ! sometimes options are added on the fly, so we can't cache it
        ### build options list
        $options = array();
        foreach($collector as $key => $value)
            $options[$key] = $value;

        #### $e and $event as function argument
        $options['e'] = $this->event(); $options['event'] = $this->event();

        $preparedCallable = \Poirot\Std\Invokable\resolveCallableWithArgs($l, $options);
        return $preparedCallable;
    }

    /**
     * Reset to emit new listener
     *
     * ! each time event try to run new listener
     *   it will clone the meeter object
     *
     * - just reset skip, with all others we can access
     *   last attributes values
     */
    function __clone()
    {
        $this->event = null;
    }
}
