<?php
namespace Poirot\Events\Event;

use Poirot\Events\Interfaces\iEvent;
use Poirot\Events\Interfaces\iMeeter;
use Poirot\Ioc\Container;
use Poirot\Ioc\instance;


/**
 * Meeter Provide Logic Of Executing Listeners
 * Out of the Events Itself.
 *
 * exp.
 *  - inject listener object dependencies
 *  - resolve argument for listeners
 *  - skip execution of listeners if collection result achieved
 *
 * @see Event::emit
 */
class MeeterIoc
    implements iMeeter
{
    /** @var Container */
    protected $ioc;
    /** @var iEvent */
    protected $event;


    /**
     * MeeterIoc constructor.
     *
     * @param Container|null $ioc
     */
    function __construct(Container $ioc = null)
    {
        $this->ioc = $ioc;
    }


    /**
     * Set Parent Event That Met
     *
     * @param iEvent $event
     * @return $this
     */
    function setEventBelong(iEvent $event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * Invoke Given Listener
     *
     * @param callable $listener
     * @param DataCollector $dataCollector Clone/Copy of Collector Instance
     *
     * @return null|array|\Traversable null mean skip result
     * @throws \Exception
     */
    function invokeListener($listener, DataCollector $dataCollector)
    {
        ### ! sometimes options are added on the fly, so we can't cache it
        ### build options list
        $options = array();
        foreach($dataCollector as $key => $value)
            $options[$key] = $value;

        #### $e and $event as function argument
        $options['e'] = $this->event; $options['event'] = $this->event;


        $callable = $listener;
        if (! is_callable($listener) )
            $callable = \Poirot\Ioc\newInitIns(new instance($listener, $options), $this->ioc);

        $callable = \Poirot\Std\Invokable\resolveCallableWithArgs($callable, $options);
        $result   = call_user_func($callable);
        return $result;
    }

    /**
     * Reset to emit new listener
     *
     * ! each time event try to run new listener
     *   it will clone the meeter object
     *
     * - just reset skip, with all others we can access
     *   last attributes values
     */
    function __clone()
    {
        $this->event = null;
    }
}
