<?php
namespace Poirot\Events\Event;

use Poirot\Events\Event;
use Poirot\Events\Interfaces\iCorrelatedEvent;
use Poirot\Events\Interfaces\iListener;
use Poirot\Events\Interfaces\iEvent;
use Poirot\Events\Interfaces\iEventHeap;
use Poirot\Std\ConfigurableSetter;

/*
new BaseEvents(new EventBuilder([
    // ---- event options
    'name' => 'application-event',
    'meeter' => $instanceOfMeeter,
    'listeners' => [
        new FunctorListener(function(){
            echo 'This is listener';
        }),
        100 => new FunctorListener(),
        'listener-name' => [
            'priority' => 100,
            'listener' => new FunctorListener(),
        ],
        150 => [
            'name'     => 'listener-othername',
            'listener' => new FunctorListener(),
            // 'priority' => 200, override config
        ],
    ],
    'then' => function(iEvent $event) { },

    // ---- events

    'binds' => [
        new BaseEvent('event-name'),
    ],
    'bind_shares' => [
        new BaseEvent('event-other-name'),
    ],
    'events' => [
        'event-name' => [
            'then' => function(iEvent $event) { },
            // and all event options
        ],
        [
            ## event name is only for identify event, it will not change event name
            'name' => 'event-other-name',
            // and all event options
        ],
    ],
]));
*/

class BuildEvent 
    extends ConfigurableSetter
{
    protected $name;
    protected $meeter;
    protected $collector;
    protected $listeners = array();
    protected $then;

    protected $binds = array();
    protected $bindShares = array();
    protected $events = array();


    /**
     * Construct
     *
     * @param array|\Traversable $options
     */
    function __construct($options = null)
    {
        // Arrange Setter Priorities
        $this->putBuildPriority(array(
            'events',
        ));

        parent::__construct($options);
    }

    /**
     * Build Events(s)
     *
     * @param iEvent|iEventHeap $event
     *
     * @return iEvent|iEventHeap
     * @throws \Exception
     */
    function build(iEvent $event)
    {
        # name
        if ($event instanceof Event && $this->name !== null)
            $event->setName($this->name);

        # meeter
        if ($this->meeter)
            $event->setMeeter($this->meeter);

        # collector
        if ($this->collector)
            $event->collector()->import($this->collector);


        # listeners
        foreach ($this->listeners as $left => $right) {
            $listener = $right;
            ## 'listener-name' => $Listener
            $name     = (is_string($left)) ? $left : false;

            // Deprecated; This not good.
            ## 100 => $Listener
            # $priority = (is_int($left)) ? $left : null /* default priority */;
            $priority = (is_int($left)) ? null : null /* default priority */;

            ## $x => new Listener
            if ($right instanceof iListener)
                $listener = $right;
            elseif (is_array($right)) {
                (! isset($right['name']) )     ?: $name     = $right['name'];
                (! isset($right['priority']) ) ?: $priority = $right['priority'];
                (! isset($right['listener']) ) ?: $listener = $right['listener'];
            }

            if (! $listener )
                throw new \Exception('Listener object not found on setter options.');

            if ($name)
                $event->attachListener($listener, $name, $priority);
            else
                $event->attachListener($listener, $priority);

        }


        # then callable
        if ($this->then)
            $event->then($this->then);

        // ................................

        if (! $event instanceof iEventHeap )
            return;

        # bind
        foreach($this->binds as $e)
            $event->bind($e);

        # bind share
        foreach($this->bindShares as $e)
            $event->bindShare($e);

        # on event attach listeners
        $agrEvents = [];
        foreach($this->events as $eventName => $eventOptions)
        {
            if ($eventOptions instanceof iCorrelatedEvent) {
                $agrEvents[] = $eventOptions;
                continue;
            }

            if (! is_string($eventName) ) {
                if (! isset($eventOptions['name']) )
                    throw new \Exception('Events name required in setter options.');

                $eventName = $eventOptions['name'];
            }

            unset($eventOptions['name']); ## event name are immutable
            __( new BuildEvent($eventOptions) )->build(
                $event->byEvent($eventName)
            );
        }


        ## Aggregate Events
        #
        /** @var iCorrelatedEvent $ag */
        foreach ($agrEvents as $ag)
            $ag->attachToEvent($event);


        return $event;
    }


    // setter options:

    /**
     * @param string $name
     */
    function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $meeter
     */
    function setMeeter($meeter)
    {
        $this->meeter = $meeter;
    }

    /**
     * @param array $listeners
     */
    function setListeners($listeners)
    {
        $this->listeners = $listeners;
    }

    /**
     * @see AbstractEvent::then
     * @param mixed $then
     */
    function setThen(callable $then)
    {
        $this->then = $then;
    }


    // iEvents

    /**
     * @param mixed $binds
     */
    function setBinds($binds)
    {
        $this->binds = $binds;
    }

    /**
     * @param mixed $bindShares
     */
    function setBindShares($bindShares)
    {
        $this->bindShares = $bindShares;
    }

    /**
     * @param mixed $events
     */
    function setEvents($events)
    {
        $this->events = $events;
    }

    /**
     * @param null $collector
     */
    function setCollector($collector)
    {
        $this->collector = $collector;
    }

    /**
     * @return null
     */
    function getCollector()
    {
        return $this->collector;
    }

    // ..

    /**
     * Build Object With Provided Options
     *
     * @param array $options        Associated Array
     * @param bool  $throwException Throw Exception On Wrong Option
     *
     * @return array Remained Options (if not throw exception)
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    function with(array $options, $throwException = true)
    {
        parent::with($options, $throwException);
    }
}
