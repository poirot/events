<?php
namespace Poirot\Events;

use Poirot\Std\Interfaces\Struct\iDataMean;
use Poirot\Std\Interfaces\Struct\iDataOptions;

use Poirot\Events\Event\BuildEvent;
use Poirot\Events\Event\Meeter;
use Poirot\Events\Event\DataCollector;
use Poirot\Events\Interfaces\iListener;
use Poirot\Events\Interfaces\iEvent;
use Poirot\Events\Interfaces\iMeeter;
use Poirot\Std\Struct\CollectionPriority;
use Poirot\Std\Struct\DataMean;


class Event
    implements iEvent
{
    // use tClone;
    
    /** @var string Events Name */
    protected $name = null;

    /** @var iDataOptions */
    protected $collector;

    /** @var \SplPriorityQueue */
    protected $listenersQueue;

    /** @var array store listeners by name */
    protected $_c_listeners = array(
        // 'listener_name' => iListener
    );

    protected $initializer;

    /** @var boolean Is Propagation Stopped By Listeners?? */
    protected $isStopPropagation = false;
    /** @var iListener Last Emitted Listener */
    protected $_c_lastEmitted;
    
    /** @var iDataMean */
    protected $meta;

    /** @var bool True While Emit Run */
    protected $_tmp__isEmiting = false;

    /** @var callable */
    protected $then;
    protected $failure;
    /** @var iMeeter */
    protected $meeter;


    /**
     * Construct
     *
     * - new Events('event-name', $setter = null)
     * with setter:
     * - new Events(new EventBuilder([ ...options]))
     *
     * @param string|BuildEvent $name
     * @param BuildEvent        $builder
     */
    final function __construct($name = null, $builder = null)
    {
        if ($name !== null) {
            if (is_string($name))
                $this->setName($name);
            elseif ($name !== null)
                $builder = $name;

            if ($builder !== null) {
                if (!$builder instanceof BuildEvent)
                    throw new \InvalidArgumentException(sprintf(
                        'Events construct give string event name or EventBuilder. given (%s)'
                        , is_object($name) ? get_class($name) : \Poirot\Std\flatten($name)
                    ));

                $builder->build($this);
            }
        }
        
        $this->__init();
    }

    /**
     * Initialize Object
     */
    protected function __init()
    {
        // Usually it can be used to attach default listeners
    }

    /**
     * Set Events Entitle Name
     *
     * - Events name are immutable
     *
     * @param string $name
     *
     * @throws \Exception
     * @return $this
     */
    function setName($name)
    {
        if ($this->name !== null)
            throw new \Exception(sprintf(
                "Event with name (%s) are immutable. can`t set name (%s)."
                , $this->name, \Poirot\Std\flatten($name)
            ));

        $this->name = (string) $name;
        return $this;
    }

    /**
     * Event Entitle Name
     * 
     * !! default name for events that has no name yet:
     *    \App\Event\Mvc\Dispatch -> App.Event.Mvc.Dispatch
     *    class                   -> name
     * 
     * @return string
     */
    function getName()
    {
        if ($this->name == '' || $this->name === null) {
            $name = str_replace('\\', '.', get_class($this));
            $this->setName($name);
        }

        return $this->name;
    }

    /**
     * To Pass Global Meta Data Between Listeners
     *
     * @return iDataMean
     */
    function meta()
    {
        if ($this->meta === null)
            $this->meta = new DataMean();

        return $this->meta;
    }
    
    /**
     * Events Result Collector
     *
     * - collector(['key_as_option' => $value])
     *
     * @param null|array|\Traversable $options
     *
     * @return DataCollector|$this
     */
    function collector($options = null)
    {
        if (! $this->collector )
            $this->collector = new DataCollector;

        if (null !== $options) {
            $this->collector->import($options);
            return $this;
        }

        return $this->collector;
    }

    /**
     * Trigger Listeners Within Events
     *
     * - reset propagation value
     * - iterate over listeners priority and fire actions
     * - readable props of this event can resolved as listener
     *   arguments.
     * - this event can be resolved to listener as $e or $event __invoke($e)
     * - implement to know last emitted listener
     *
     * note: collecting listeners results can implemented with meeter watch
     * note: each event after emit has collected of data result in Emitter
     *       Collector. if you want it can be merged with event collector.
     *       $results = $emitter->collector()
     *       $emitter->event()->collector()->from($result)
     *
     * @param array|\Traversable|iMeeter $dataMerge
     * @param iMeeter|null $meeter
     *
     * @return iEvent Clone/Copy of self
     * @throws \Exception
     */
    function emit($dataMerge = null, $meeter = null)
    {
        # Prepare given arguments:
        if ($dataMerge instanceof iMeeter) {
            $meeter = $dataMerge;
            $dataMerge = null;
        }

        $Event     = clone $this;
        $Collector = $Event->collector();
        if ($dataMerge !== null)
            ## build collector with default data
            ## we have to keep defaults untouched
            $Collector->import($dataMerge);

        if ($meeter === null)
            $meeter = $this->_getMeeter();


        # Emit listeners:
        $this->_tmp__isEmiting = true;

        $result = null;
        /** @var iListener $listener */
        foreach(clone $this->_listenerQueue() as $listener)
        {
            # propagation stopped by listener
            if ( $Event->isStopPropagation() )
                break;


            $this->_c_lastEmitted = $listener;

            /** @var iMeeter $meeter */
            $meeter = clone $meeter; // reset meter to execute new listener
            $meeter->setEventBelong($Event);

            try {
                $result = $meeter->invokeListener($listener, $Collector);
            } catch(\Exception $e) {
                if ($this->failure)
                    return call_user_func($this->failure, $e, $Event);
                
                throw $e;
            }

            if ($result !== null) {
                if (is_array($result) || $result instanceof \Traversable)
                    ## set result into collector
                    $Collector->import($result);

                // TODO determine result to merge with Collector \
                //      or store in collector with listener name.  
            }
        }

        $this->_tmp__isEmiting = false;
        return $Event;
    }

    /**
     * callable:
     *  mixed function(DataCollector $result, $eventSelf)
     *  
     * @param callable $callable
     * @param callable $failure
     * 
     * @return mixed|$this if callable not given
     * @throws \Exception
     */
    function then(/*callable*/ $callable = null, /*callable*/ $failure = null)
    {
        if ($failure !== null)
            $this->onFailure($failure);

        if ($callable === null)
            return $this;
        
        
        if (! is_callable($callable) )
            throw new \Exception(sprintf(
                'The argument given is not callable; given: (%s).'
                , \Poirot\Std\flatten($callable)
            ));


        try {
            $result = call_user_func($callable, $this->collector(), $this);
        } catch(\Exception $e) {
            if ($this->failure)
                return call_user_func($this->failure, $e, $this);

            throw $e;
        }
        
        $this->then = $callable;
        return $result;
    }

    /**
     * callable:
     *  mixed function(\Exception $exception, $eventSelf)
     * 
     * @param callable|null $callable
     * 
     * @return $this
     * @throws \Exception
     */
    function onFailure(/*callable*/$callable)
    {
        if ($callable === null) {
            $this->failure = $callable;
            return $this;
        }

        if (!is_callable($callable))
            throw new \Exception(sprintf(
                'The argument given is not callable; given: (%s).'
                , \Poirot\Std\flatten($callable)
            ));

        
        $this->failure = $callable;
        return $this;
    }
    
    /**
     * Attach Listener To This Events
     *
     * [code:]
     *   attachListener($listener, $priority)
     *   attachListener($listener, $nameOverride, $priority)
     * [code]
     *
     * - store attached listener so we can list theme back
     * - listener with same name ({$name}) not allowed
     * - can`t Attach Listener While Emit Events
     *
     * @param callable    $listener
     * @param int |string $name      priority, null | name, priority
     * @param null|int    $priority            null |       priority
     *
     * @throws \RuntimeException Listener exists
     * @return $this
     */
    function attachListener($listener, $name = null, $priority = 10)
    {
        if ($priority == null)
            $priority = 10;

        if ($this->_tmp__isEmiting)
            throw new \RuntimeException('Can`t Attach Listener While Emit Events.');

        if ( is_int($name) ) {
            ## listener without specified name
            $priority = $name;
            $name     = null;
        }

        if ($name === null) {
            ## achieve default name if given listener is class
            if (is_object($listener) && !$listener instanceof \Closure)
                $name = get_class($listener);
        }
        
        if ($name !== null) {
            ## store listener with name, listener is not anonymous!
            if (isset($this->_c_listeners[$name]))
                throw new \RuntimeException("Listener with same name ({$name}) not allowed.");

            $this->_c_listeners[$name] = $listener;
        }

        $this->_listenerQueue()->insert($listener, $priority);
        return $this;
    }
    
    /**
     * Find For Listener with name or object
     *
     * @param iListener|string $listener
     *
     * @throws \InvalidArgumentException
     * @return iListener|false
     */
    function findListener($listener)
    {
        if (is_string($listener))
            return (isset($this->_c_listeners[$listener])) ? $this->_c_listeners[$listener] : false;
        elseif (is_object($listener)) {
            foreach (clone $this->_listenerQueue() as $l) {
                if ($l === $listener)
                    return $listener;
            }
        }
        
        return false;
    }
    
    /**
     * Detach A Listener From List
     *
     * - to detach by name you have to findListener() first
     *
     * @param iListener $listener
     *
     * @return $this
     */
    function detachListener(iListener $listener)
    {
        $this->_listenerQueue()->del($listener);

        if (in_array($listener, $this->_c_listeners)) {
            foreach ($this->_c_listeners as $k => $l) {
                if ($l === $listener) {
                    unset($this->_c_listeners[$k]);
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * Get All Attached Listeners To This Events
     *
     * - listeners list must contain both
     *   priority and listener object
     *
     * note: can used for debug purposes
     *
     * @return iListener[]
     */
    function listListeners()
    {
        $listeners = array();
        foreach (clone $this->_listenerQueue() as $l) 
            $listeners[] = $l;
        
        return $listeners;
    }

    /**
     * Detach Whole Listeners
     *
     * @return $this
     */
    function detachAll()
    {
        $this->_listenerQueue()->clean();

        # clean listeners name store
        $this->_c_listeners = array();
        return $this;
    }

    /**
     * Stop Listeners Propagation
     *
     * @return $this
     */
    function stopPropagation()
    {
        $this->isStopPropagation = true;
        return $this;
    }

    /**
     * Is Stopped?
     *
     * @return boolean
     */
    function isStopPropagation()
    {
        return $this->isStopPropagation;
    }

    /**
     * Get Last Emitted Listener Object
     *
     * @return null|iListener
     */
    function lastEmitted()
    {
        return $this->_c_lastEmitted;
    }


    // Options

    /**
     * Set Default Meeter Into Emit Method
     *
     * @param iMeeter $meeter
     *
     * @return $this
     */
    function setMeeter(iMeeter $meeter)
    {
        $this->meeter = $meeter;
        return $this;
    }


    // ...

    private function _getMeeter()
    {
        if (! $this->meeter)
            $this->setMeeter(new Meeter);

        return $this->meeter;
    }

    protected function _listenerQueue()
    {
        if (!$this->listenersQueue)
            $this->listenersQueue = new CollectionPriority;

        return $this->listenersQueue;
    }


    function __clone()
    {
        $_f__clone_array = function($arr) use (&$_f__clone_array) {
            foreach ($arr as &$v) {
                if (is_array($v))
                    $_f__clone_array($v);
                elseif (is_object($v))
                    $v = clone $v;
            }
        };

        foreach($this as &$val) {
            if (is_array($val))
                $_f__clone_array($val);
            elseif (is_object($val))
                $val = clone $val;
        }
    }
}
