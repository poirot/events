<?php
namespace Poirot\Events;

use Poirot\Events\Interfaces\iEvent;
use Poirot\Events\Interfaces\iEventHeap;
use Poirot\Events\Interfaces\iMeeter;
use Poirot\Std\Interfaces\Struct\iData;


/**
 * Other Events Classes Can Extend This Class
 *
 * exp. we can have ApplicationEvents
 *      that contains:
 *        BootstrapEvent -> with own options meta data and listeners attached into
 *        RouteEvent ...
 *        ...
 *        RenderEvent
 *        .
 *        .
 */
class EventHeap
    extends Event
    implements iEventHeap
{
    protected $attached_events = array();
    protected $isShutdown = false;


    /**
     * Bind an event to this event
     *
     * @param iEvent      $event
     *
     * @throws \Exception If Events With Name Exists
     * @return $this
     */
    function bind(iEvent $event)
    {
        $eventName = $event->getName();
        if ($this->has($event))
            throw new \Exception(sprintf(
                'Events with name "%s" is registered before.'
                , $eventName
            ));


        if ($this->meeter !== null)
            $event->setMeeter($this->meeter);

        $this->attached_events[$eventName] = $event;
        return $this;
    }

    /**
     * Bind an event to this event and share collector
     *
     * @param iEvent $event
     *
     * @return $this
     */
    function bindShare(iEvent $event)
    {
        $event->collector()->import($this->collector());
        $this->bind($event);

        return $this;
    }

    /**
     * Helper To Attach Listener To Events(s).
     *
     * ! to attach listener to all events use getEvents method
     *
     * - if iEvent passed as argument register it if not exists
     *
     *  - on(['login', 'logout'], $logUserAccess)
     *  - on('login', $logUserAccess)
     *  - on(iEvent, ..)
     *
     * @param array|string|iEvent $event
     * @param callable            $listener
     * @param int $priority
     *
     * @throws \Exception event name not registered
     * @return $this
     */
    function on($event, $listener, $priority = 0)
    {
        if (is_string($event) || $event instanceof iEvent)
            $event = array($event);
        elseif (!is_array($event))
            throw new \InvalidArgumentException(sprintf(
                'Events must be a string or iEvent instance, or array of these two.'
            ));

        foreach($event as $e) {
            $this->byEvent($e)->attachListener($listener, $priority);
        }

        return $this;
    }

    /**
     * Helper To Emit Events Listener
     *
     * - if iEvent passed as argument register it if not exists
     *
     *  - trigger(['login', 'logout'], $logUserAccess)
     *  - trigger('login', $logUserAccess)
     *  - trigger(iEvent, ..)
     *
     * note: return current emitter
     *
     * @param array|string|iEvent $event
     * @param array|iData|iMeeter $bp
     * @param iMeeter|null $pb
     *
     * @return iEventHeap
     * @throws \Exception
     */
    function trigger($event, $bp = null, $pb = null)
    {
        if ( $this->isShutdown() )
            return $this;


        $Collector = $this->collector();
        
        if ($bp === null)
            ## emit nested event with current collector data
            $bp = $Collector;
        elseif ($bp instanceof iData || is_array($bp))
            /** @var iData $Collector */
            $bp = $Collector->import($bp);

        if (is_string($event) || $event instanceof iEvent)
            $event = array($event);
        else if (!is_array($event))
            throw new \InvalidArgumentException(sprintf(
                'Events must be a string or iEvent instance, or array of these two.'
            ));


        foreach($event as $e)
        {
            $event = $this->byEvent($e);
            $event = clone $event;
            try {
                $event = $event->emit($bp, $pb);
            } catch(\Exception $exception) {
                /**
                 * function ($e) use ($events) {
                 *     $events->shutdown();
                 *     throw new \Exception();
                 * }
                 *
                 * in some cases that we don't want event handle failure
                 * for example it happens for me while requests made to system
                 * failed on every request and there is no way to generate something to user
                 * it's a critical error so with shutdown and throw just display user a simple
                 * default error page instead of provide it with render event
                 */
                if ($this->failure && !$this->isShutdown()) {
                    call_user_func($this->failure, $exception, $this);
                    return $this;
                }

                throw $exception;
            }

            $Collector->import( $event->collector() );
        }

        return $this;
    }

    /**
     * Shutdown all listeners attached to all events
     *
     * @return $this
     */
    function shutdown()
    {
        foreach ($this->listEvents() as $event)
            $this->byEvent($event)->stopPropagation();

        $this->stopPropagation();

        $this->isShutdown = true;
        return $this;
    }

    /**
     * Is Shutdown?
     *
     * @return bool
     */
    function isShutdown()
    {
        return $this->isShutdown;
    }

    /**
     * Un-Register an event
     *
     * @param string|iEvent $event
     *
     * @return $this
     */
    function unbind($event)
    {
        (!$event instanceof iEvent) ?: $event = $event->getName();
        unset($this->attached_events[(string) $event]);

        return $this;
    }

    /**
     * Find an event object by name
     *
     * - if iEvent passed as first argument register
     *   and return it
     *
     * @param string|iEvent $event
     *
     * @throws \Exception event not found
     * @return iEvent
     */
    function byEvent($event)
    {
        if (! $this->has($event) ) {
            if ($event instanceof iEvent)
                $this->bind($event);
            else 
                throw new \Exception("Events ({$event}) not found.");
        }

        $event = ($event instanceof iEvent) ? $event->getName() : $event;
        /** @var iEvent $event */
        $event = $this->attached_events[$event];
        if ($this->meeter)
            // Use Same Meeter For Bind Events
            $event->setMeeter($this->meeter);

        return $event;
    }

    /**
     * Has Events
     *
     * @param string|iEvent $event
     *
     * @return boolean
     */
    function has($event)
    {
        $event = ($event instanceof iEvent)
            ? $event->getName()
            : (string) $event;

        return array_key_exists($event, $this->attached_events);
    }

    /**
     * Get All Registered Events Name
     *
     * note: mostly used for debug purposes
     *
     * @return array[string]
     */
    function listEvents()
    {
        return array_keys($this->attached_events);
    }
}
